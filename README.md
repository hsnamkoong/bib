## Background

This is Hong and collaborators' bibliography file, which builds off of John Duchi's, itself seeded by that of Rob Schapire and Yoram Singer's. The file adheres to a particular naming convention to make citations easier across multiple projects, and contains an exceedingly large number of entries. Please be responsible in maintaining its sanity. More than a decade's worth of effort has gone into maintaining this central repository file; the bib file was 13 years old when I forked it off of John's after graduating from Stanford in 2019. I expect you to continue this effort.


This central bib.bib file is very convenient as it removes the need to maintain multiple bibliography files for each project. If you are a student, you will quickly realize that this approach is scalable as your research operation grows (i.e. once you have more than two concurrent projects). Please do not start your own bib file, and use this one---it already contains a vast set of references.

---

## Using the file: soft links

You can use the file by soft-linking the bib.bib file into your project repository. To soft-link, use the command

ln -s [path-to-bib]/bib.bib [path-to-desired-place].

Then, you can refer to this the [path-to-desired-place] in your latex file.

---

## Entry formatting

The file satisfies aggressively precise formatting rules, so that papers are easy to search and cite within the file.

Entries are in alphabetical order by first author's last name, and each has a key given by first author's full last name, then first two initials of each additional author, followed by the 2-digit year. As a typical example, here is

@article{DuchiBaWa12, author = {John C. Duchi and Peter L. Bartlett and Martin J. Wainwright}, title = {Randomized Smoothing for Stochastic Optimization}, year = 2012, journal = siopt, volume = 22, number = 2, pages = {674--701}, }

Note that this has an abbreviated journal. This is because there are strings at the top of the file to allow consistent formatting. Please use those whenever possible; e.g., NIPS should be

booktitle = nips2013

where the last four digits indicate which year of NIPS it is.

---

## Arxiv papers

When entering things from the arXiv, use the format

@article{DuchiGlNa16, title={Statistics of Robust Optimization: A Generalized Empirical Likelihood Approach}, author={John C. Duchi and Peter W. Glynn and Hongseok Namkoong}, year=2016, journal={arXiv:1610.03425 [stat.ML]}, }

Note that you should never use

journal = {arXiv preprint arXiv:1610.03425}

as that does not actually reflect arXiv's desired citation style. You should include the subject area. Relatedly, do not simply download Google's citations, as they are typically wrong and have bad capitalization, etc.

## Comments and Common Errors

In the title field, any letters that should be capitalized must be surrounded by brackets, i.e.

title = {Some cool things called {M}arkov chains}

rather than

title = {Some cool things called Markov chains}

as latex will lower-case Markov in the second example.
